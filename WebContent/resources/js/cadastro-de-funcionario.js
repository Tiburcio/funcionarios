var inicio = new Vue({
	el:"#inicio",
    data: {
        listaProdutos: [],
		funcionario,
		listaFuncionario,
        listaProdutosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "fabricante.nome", label:"Fabricante"},
			{sortable: false, key: "volume", label:"Volume"},
			{sortable: false, key: "unidade", label:"Unidade"},
			{sortable: false, key: "estoque", label:"Estoque"}
		]
    },
    created: function(){
        let vm =  this;
        vm.incluirFuncionario();
		vm.listarFuncionario();
    },
    methods:{
        incluirFuncionario: function(funcionario){
			const vm = this;
			axios.post("/funcionarios/", funcionario)
			.then(response => {vm.funcionario = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		listarFuncionario: function(funcionario){
			const vm = this;
			axios.get("/funcionarios/")
			.then(response => {vm.listaFuncionarios = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
    }
});
