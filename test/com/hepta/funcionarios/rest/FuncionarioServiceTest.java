package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.funcionarios.entity.Funcionario;

class FuncionarioServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}
	
	@Test
	void testFuncionarioCreate() throws Exception {
		FuncionarioService funcionario = new FuncionarioService();
		Funcionario func = new Funcionario();
		func.setId(1);
		func.setNome("Joao");
		func.setSetor(null);
		func.setSalario(1000D);
		func.setEmail("cleo@hepta.com.br");
		func.setIdade(25);
		funcionario.FuncionarioCreate(func);
	}

	@Test
	void testFuncionarioRead() {
		fail("Not yet implemented");
	}

	@Test
	void testFuncionarioUpdate() {
		fail("Not yet implemented");
	}

	@Test
	void testFuncionarioDelete() {
		fail("Not yet implemented");
	}

}
