## Para que esse projeto funcionasse tive de configurar todo o ambiente de trabalho localhost do início ao fim, pois não tinha nenhum aplicativo, nem o eclipse e nem o mysql instalado.

- Ao clonar o projeto, importei o mesmo com o Maven;
- Dei o Maven > Update Project para baixar as dependências do pom.xml;
- Também tive que baixar e configurar o apache tomcat v9.0

## Esse projeto segue o fluxo git flow de trabalho
## Para o projeto funcionar foi adicionado uma dependencia no pom.xml, que faltava(Pois estava dando erro):

<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
    <version>2.3.1</version>
</dependency>
 
 ## Como esse projeto usa o JPA, então para o banco de dados:

Apenas criei o banco de dados: funcionarios_prova

Pois no persistence.xml já estava configurado para gerar as tabelas automáticamente:
## property name="hibernate.hbm2ddl.auto" value="update"

Em seguida realizei 2 inserts diretos na base para verificar o funcionamento nas tabelas:
 - funcionario e setor
## Acesso ao Swagger (inicio)

http://localhost:8080/documentacao/

## Testes Unitários Utilizando (Iniciei implementacao)

Junit -> Basta clicar na classe de teste -> Coverage As -> JUnit test

Nesse início foi apenas 17% de cobertura.
